package com.billclinton.taskbeeper.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TimePicker;

import com.billclinton.taskbeeper.R;


public class AddTaskActivity extends BaseActivity {

    CalendarView calendar;
    TimePicker timePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        calendar = findViewById(R.id.activity_add_task_calendar);
        timePicker = findViewById(R.id.activity_add_task_timePicker);

        toolbar.setNavigationIcon(android.R.drawable.ic_delete);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
        getSupportActionBar().setTitle("New Task");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
