package com.billclinton.taskbeeper.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.billclinton.taskbeeper.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    ListView list;
    List<String> arrayList;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Tasks");

        list = findViewById(R.id.activity_main_list);

        // For testing purposes
        arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add("Name " + String.valueOf(i));
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayList);

        list.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.activity_main_addTask) {
            // TODO:: Make this a startActivity result
            startActivity(new Intent(this, AddTaskActivity.class));
        }

        return true;
    }
}

// TODO:: Make View modes -- 2 modes, 1: List View mode, 2: Calendar mode
// TODO:: Setup Android Room for local database
// TODO:: Setup for notifications when alarm is triggered